<?php
namespace PK;

class Bootstrap {
    public function __construct()
    {
        if (isset($_GET['url']))
            $url = explode('/', rtrim($_GET['url'], '/'));

        if (isset($url)) {
            if (!file_exists(dirname(__DIR__) . '/app/Controllers/' . $url[0] . '.php')) return http_response_code(404);
            require 'app/Controllers/' . $url[0] . '.php';
            $ControllerName = '\\PK\\' . $url[0];
            $Controller = new $ControllerName;
            if (isset($url[1])){
                $Variables = [];
                for ($i = count($url); $i > 1; $i--)
                    $Variables[] = $url[$i];
                array_reverse($Variables);
                $Controller->{$url[1]}(...$Variables);
            }

        }
        elseif (!isset($url))
            echo 'You are in fucking homepage';
    }
}