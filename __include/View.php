<?php
namespace PK;

class View {
    public function __construct()
    {
    }

    public function view($name) {
        require 'resources/' . $name . '.php';
    }
}